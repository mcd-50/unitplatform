var expect = require("chai").expect;
var request = require("request");

// eslint-disable-next-line no-undef
describe("Deposit placing", function () {
	// customer id
	var bcustomerId = "e2897eb0-5c67-11ea-a89d-8b779e37a5f8";

	// token
	var bjwt = "Basic dGVzdGluZ0F1dGg6dGVzdGluZ0BBdXRo";
	
	const bpayload = {
		customerId: bcustomerId,
		amount: "0.1",
		currencyType: "eth",
		transactionHash: Math.random().toString().slice(2, 14),
		status: "completed",
		baseObjectId: Math.random().toString().slice(2, 14) + Math.random().toString().slice(2, 14)
	};

	const cheaders = {
		"Accept": "application/json",
		"Content-Type": "application/json",
	};

	const rbpayload = {
		method: "post",
		uri: "http://13.235.232.185:14001/api/microservice/createDeposit",
		body: {
			payload: { ...bpayload }
		},
		headers: {
			...cheaders,
			Authorization: bjwt,
		},
		json: true
	};

	// eslint-disable-next-line no-undef
	it("deposit eth", function (done) {
		// eslint-disable-next-line no-unused-vars
		request(rbpayload, function (error, response, body) {
			expect(response.statusCode).to.equal(200);
			done();
		});
	});
});