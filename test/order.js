var expect = require("chai").expect;
var request = require("request");

// eslint-disable-next-line no-undef
describe("Order placing", function () {

	// customer id
	var bcustomerId = "e2897eb0-5c67-11ea-a89d-8b779e37a5f8";
	var scustomerId = "bab19a90-61e8-11ea-9a1d-7969c01d3e80";

	// token
	var bjwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlSWQiOiI3OTFGRDBCM0E5IiwiY3VzdG9tZXJJZCI6ImUyODk3ZWIwLTVjNjctMTFlYS1hODlkLThiNzc5ZTM3YTVmOCIsImVtYWlsIjoiYXl1c2guYXMucy5odWtsYUBnbWFpbC5jb20iLCJtb2JpbGUiOiI3OTFGRDBCM0E5IiwiY3VzdG9tZXJUeXBlIjoiY3VzdG9tZXIiLCJhdXRoUHJlZmVyZW5jZSI6ImVtYWlsIiwiaWF0IjoxNTgzODE4NDM4LCJleHAiOjE1ODY0MTA0Mzh9.ddv3UbMfJ2ZdjG4uXAPQVEVW1JDHajYxuHVrkoiEjws";
	var sjwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlSWQiOiIxRTMxQTM0RDI2IiwiY3VzdG9tZXJJZCI6ImJhYjE5YTkwLTYxZTgtMTFlYS05YTFkLTc5NjljMDFkM2U4MCIsImVtYWlsIjoiYXl1c2guYXMuc2gudWtsYUBnbWFpbC5jb20iLCJtb2JpbGUiOiIxRTMxQTM0RDI2IiwiY3VzdG9tZXJUeXBlIjoiY3VzdG9tZXIiLCJhdXRoUHJlZmVyZW5jZSI6ImVtYWlsIiwiaWF0IjoxNTgzODE4NDc3LCJleHAiOjE1ODY0MTA0Nzd9.LJNYUvOlM_iGUMMUEIEcmvG-j2QON955VpDigCW3XHI";

	const bpayload = {
		customerId: bcustomerId,
		baseCurrencyType: "usdt",
		currencyType: "eth",
		type: "buy",
		quantity: String(0.01),
		price: String(129)
	};

	const spayload = {
		customerId: scustomerId,
		baseCurrencyType: "usdt",
		currencyType: "eth",
		type: "sell",
		quantity: String(0.01),
		price: String(129)
	};

	const cheaders = {
		"Accept": "application/json",
		"Content-Type": "application/json",
	};

	const rbpayload = {
		method: "post",
		uri: "http://13.235.232.185:14001/api/createOrder",
		body: {
			payload: { ...bpayload }
		},
		headers: {
			...cheaders,
			Authorization: bjwt,
		},
		json: true
	};

	const rspayload = {
		method: "post",
		uri: "http://13.235.232.185:14001/api/createOrder",
		body: {
			payload: { ...spayload }
		},
		headers: {
			...cheaders,
			Authorization: sjwt,
		},
		json: true
	};

	// eslint-disable-next-line no-undef
	it("buy order", function (done) {
		// eslint-disable-next-line no-unused-vars
		request(rbpayload, function (error, response, body) {
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// eslint-disable-next-line no-undef
	it("sell order", function (done) {
		// eslint-disable-next-line no-unused-vars
		request(rspayload, function (error, response, body) {
			expect(response.statusCode).to.equal(200);
			done();
		});
	});
});