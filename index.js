//Load express module with `require` directive
var express = require("express");
var app = express();
var mocha = require("mocha");

// eslint-disable-next-line no-unused-vars
var _mocha = new mocha({
	reporter: "mochawesome",
	reporterOptions: {
		overwrite: true,
		reportTitle: "Testing the system",
	}
});

//Define request response in root URL (/)
app.get("/", function (req, res) {
	res.send("Hello World");
});

//Launch listening server on port 8080
app.listen(8080, function () {
	console.log("App listening on port 8080!");
});